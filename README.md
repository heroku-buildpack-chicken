# Heroku Buildpack: CHICKEN Scheme

This is a [Buildpack][] for deploying [CHICKEN Scheme][chicken] apps to
[Heroku][].

It comes with CHICKEN [4.13.0][releases] and uses the [egg][] packaging
infrastructure to manage dependencies.

**This project is no longer maintained.** It may not work, and even if it does,
it doesn't support any versions of CHICKEN newer than 4.13.0. You probably
don't want to use it, or at least not without updating it first.

## Usage

    $ git ls-files
    Procfile
    deploy.meta
    run.scm

    $ cat deploy.meta
    ((description "An example CHICKEN app for Heroku")
     (depends awful))

    $ cat run.scm
    (use awful)
    (define-page (main-page-path)
      (lambda () "Hello World!"))

    $ cat Procfile
    web: awful --port=$PORT run.scm

    $ heroku apps:create --buildpack https://bitbucket.org/evhan/heroku-buildpack-chicken.git
    ...

    $ git push heroku master
    ...
    remote: -----> CHICKEN app detected
    remote: -----> Installing CHICKEN 4.13.0
    remote: -----> Installing eggs
    ...
    remote: -----> Copying CHICKEN onto slug
    remote: -----> Configuring default environment
    remote: -----> Discovering process types
    remote:        Procfile declares types -> web
    remote: -----> Compressing...
    remote:        Done: 5.7M
    remote: -----> Launching...
    remote:        Released v1
    remote:        http://deep-journey-2786.herokuapp.com deployed to Heroku

To be recognized as a CHICKEN application, your project must have a `run.scm`,
`deploy.meta` or `deploy.setup` file in its root. If no [`Procfile`][procfile]
is included in the app, `run.scm` will be run as the default `web` process.

If a `deploy.meta` file is present, dependencies listed therein will be
installed onto your slug during the deploy. This file should follow
CHICKEN's [metafile][] format.

If a `deploy.setup` file is present, it will be run by `chicken-install` during
the deploy. This file should follow CHICKEN's [setupfile][] format.

The vendored CHICKEN and installed eggs are cached between deploys. If a
`deploy.setup` file is present, however, it is *always* rerun.

A specific version of CHICKEN to install can be defined in the metafile:

    $ cat deploy.meta
    ((description "An example CHICKEN app for Heroku")
     (depends awful)
     (chicken 4.12.0))

The following CHICKEN releases are available in this way:

  * 4.8.0
  * 4.8.0.1
  * 4.8.0.2
  * 4.8.0.3
  * 4.8.0.4
  * 4.8.0.5
  * 4.8.0.6
  * 4.9.0
  * 4.9.0.1
  * 4.10.0
  * 4.11.0
  * 4.12.0
  * 4.13.0

[Buildpack]: https://devcenter.heroku.com/articles/buildpacks
[chicken]: https://call-cc.org/
[Heroku]: https://heroku.com/
[releases]: https://code.call-cc.org/releases
[egg]: https://wiki.call-cc.org/eggs
[procfile]: https://devcenter.heroku.com/articles/procfile
[metafile]: https://wiki.call-cc.org/eggs%20tutorial#the-meta-file
[setupfile]: https://wiki.call-cc.org/eggs%20tutorial#the-setup-file
